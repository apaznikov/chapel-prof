#include "stdio.h"
#include "sys/time.h"
#include "time.h"

typedef unsigned long int int32_t;
typedef const char* chpl_string;

struct timeval prog_end, main_start;
extern int chpl_nodeID, chpl_numNodes;

void __real_chpl_comm_init(int *argc_p, char ***argv_p);
void __real_chpl_exit_all(int status);
int __real_main(int argc, char* argv[]);
void __real_chpl_comm_get(void *addr, int32_t locale, void* raddr,
                     	  int32_t elemSize, int32_t typeIndex, int32_t len,
                          int ln, chpl_string fn);
void __real_chpl_comm_put(void *addr, int32_t locale, void* raddr,
                          int32_t elemSize, int32_t typeIndex, int32_t len,
                          int ln, chpl_string fn);

/* 
 * __wrap_chpl_comm_init: Wrapper for chpl_comm_init. 
 *                        Output base information. 
 */
void __wrap_chpl_comm_init(int *argc_p, char ***argv_p)
{
  __real_chpl_comm_init(argc_p, argv_p);
  FILE *fp;
  fp = fopen("results/info", "w");
  fprintf(fp, "%d\n", chpl_numNodes);
  fclose(fp);
}

/* 
 * __wrap_chpl_exit_all: Wrapper for chpl_exit_all. 
 *                       Output total execution time. 
 */
void __wrap_chpl_exit_all(int status)
{
  FILE *fp;

  fp = fopen("results/info", "a");
  gettimeofday(&prog_end, NULL);

  if(chpl_nodeID == 0) {
    fprintf(fp, "%.3f\n", prog_end.tv_sec - main_start.tv_sec +
            (prog_end.tv_usec - main_start.tv_usec) * 0.000001);
  }
  fclose(fp);

  __real_chpl_exit_all(status);
}

/* 
 * __wrap_main: Wrapper for main. 
 *                       Save time of execution beginning. 
 */
void __wrap_main(int argc, char* argv[])
{
  gettimeofday(&main_start, NULL);
  __real_main(argc, argv);
}

/* _wrap_begin: */
#define _wrap_begin(chpl_func)                                                 \
  struct timeval start, finish;                                                \
  FILE *fp;                                                                    \
  double time_from_start, op_time, start_time, finish_time;                    \
  char fname[30];                                                              \
                                                                               \
  sprintf(fname, "results/%s%d", chpl_func, chpl_nodeID);                      \
                                                                               \
  fp = fopen(fname, "a");                                                      \
                                                                               \
  if (fp == NULL) {                                                            \
      fprintf(stderr, "can't open file %s", fname);                            \
  }                                                                            \
                                                                               \
  gettimeofday(&start, NULL);                                       

/* _wrap_end: */
#define _wrap_end(op_name)                                                     \
  gettimeofday(&finish, NULL);                                                 \
                                                                               \
  op_time = finish.tv_sec - start.tv_sec;                                      \
  op_time += (finish.tv_usec - start.tv_usec) * 0.000001;                      \
                                                                               \
  start_time = start.tv_sec - main_start.tv_sec;                               \
  start_time += (start.tv_usec - main_start.tv_usec) * 0.000001;               \
                                                                               \
  finish_time = finish.tv_sec - main_start.tv_sec;                             \
  finish_time += (finish.tv_usec - main_start.tv_usec) * 0.000001;             \
                                                                               \
  time_from_start = start.tv_sec - main_start.tv_sec;                          \
  time_from_start += (start.tv_usec - main_start.tv_usec) * 0.000001;          \
                                                                               \
  fprintf(fp, "%d %d %d %d %s %lf %lf %lf %s\n",                               \
	      chpl_nodeID, locale, elemSize, len,                                  \
          op_name, start_time, finish_time, op_time, fn);                      \
                                                                               \
  fclose(fp);

/* 
 * __wrap_chpl_comm_get: Wrapper for chpl_comm_get. 
 */
void __wrap_chpl_comm_get(void *addr, int32_t locale, void* raddr,
                          int32_t elemSize, int32_t typeIndex, int32_t len,
                          int ln, chpl_string fn)
{
  _wrap_begin("chpl_comm_get");

  __real_chpl_comm_get(addr, locale, raddr, elemSize, typeIndex, len, ln, fn);

  _wrap_end("get");

  return;
}

/* 
 * __wrap_chpl_comm_get: Wrapper for chpl_comm_put. 
 */
void __wrap_chpl_comm_put(void *addr, int32_t locale, void* raddr,
                          int32_t elemSize, int32_t typeIndex, int32_t len,
                          int ln, chpl_string fn)
{
  _wrap_begin("chpl_comm_put");

  __real_chpl_comm_put(addr, locale, raddr, elemSize, typeIndex, len, ln, fn);

  _wrap_end("put");

  return;
}
