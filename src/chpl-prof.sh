#!/bin/bash

prog=`echo $1 | sed s/.chpl$//`
name="wrappers.c"
foo1="chpl_comm_get"
foo2="chpl_comm_put"
foo3="main"
foo4="chpl_exit_all"
foo5="chpl_comm_init"
options="-Xlinker --wrap=$foo1 -Xlinker --wrap=$foo2 \
         -Xlinker --wrap=$foo3 -Xlinker --wrap=$foo4 \
         -Xlinker --wrap=$foo5"

#
# Prepare: clean all and compile Chapel program
#

mkdir -p results
#rm -f results/* $prog $prog_real
rm -f results/*
chpl $prog.chpl --fast -o $prog $name --ldflags "$options"

#
# Make TORQUE job file
#

job=$prog-prof.job
>$job
echo "#PBS -N $prog-chpl-prof" >>$job
echo "#PBS -l nodes=4:ppn=8"   >>$job
echo "#PBS -j oe"              >>$job
echo "#PBS -q release"         >>$job

echo "cd \$PBS_O_WORKDIR"       >>$job

echo "chplrun ./$prog"         >>$job

#qsub h.job
