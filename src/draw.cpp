#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <cstdlib>

using namespace std;

const int XTICS_SCALE = 43;
const int IMG_WIDTH   = 1024;
const int IMG_HEIGHT  = 768;
const int FONT_SIZE   = 14;

int read_nnodes();
int **read_matrix(string file_templ, int nnodes);
void write_matrix(int **comm_matrix, int nnodes, string fname);
void write_gpfile(string gpfname, string pngfile, string datfile, int nnodes,
                  double scale_factor);
double compute_scale_factor(int **matrix, int nnodes);

int main(int argc, const char *argv[])
{
    try {
        int nnodes = read_nnodes();

        int **get_matrix = read_matrix("results/chpl_comm_get", nnodes);
        int **put_matrix = read_matrix("results/chpl_comm_put", nnodes);

        double scale_factor_get = compute_scale_factor(get_matrix, nnodes);
        double scale_factor_put = compute_scale_factor(put_matrix, nnodes);

        write_matrix(get_matrix, nnodes, "results/get.dat");
        write_matrix(put_matrix, nnodes, "results/put.dat");

        write_gpfile("results/get.gp", "get.png", "get.dat", 
                     nnodes, scale_factor_get);
        write_gpfile("results/put.gp", "put.png", "put.dat", 
                     nnodes, scale_factor_put);

        system("cd results && gnuplot *.gp && cd ..");
    }

    catch (runtime_error &e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    }

    return 0;
}

// read_nnodes
int read_nnodes()
{
    ifstream info("results/info");
    if (info.fail()) {
        throw runtime_error("error at open results/info");
    }

    int nnodes;
    info >> nnodes;
    info.close();
    return nnodes;
}

// read_matrix
int **read_matrix(string file_templ, int nnodes)
{
    int **comm_matrix = new int*[nnodes];
    for (int i = 0; i < nnodes; i++) {
        comm_matrix[i] = new int[nnodes];
    }

    // init matrix
    for (int i = 0; i < nnodes; i++) {
        for (int j = 0; j < nnodes; j++) {
            comm_matrix[i][j] = 0;
        }
    }

    // read data and fill the matrix
    for (int i = 0; i < nnodes; i++) {
        stringstream sstrm;
        sstrm << file_templ << i;
        ifstream node_file(sstrm.str().c_str());
        if (node_file.fail()) {
            // throw runtime_error("error at open node_file");
            cerr << "error at open node_file" << endl;
            continue;
        }

        string tmpline;
        do {
            int aside, bside;
            node_file >> aside >> bside;
            comm_matrix[aside][bside]++;
        } while (getline(node_file, tmpline));    // read rest of line

        node_file.close();
    }

    return comm_matrix;

}

// write_matrix
void write_matrix(int **comm_matrix, int nnodes, string fname)
{
    ofstream ofile(fname.c_str());
    if (ofile.fail()) {
        throw runtime_error("error at open ofile");
    }

    for (int i = 0; i < nnodes; i++) {
        for (int j = 0; j < nnodes; j++) {
            cout << comm_matrix[i][j] << " ";
            ofile << comm_matrix[i][j] << " ";
        }
        cout << endl;
        ofile << endl;
    }
    cout << endl;
    ofile.close();

    for (int i = 0; i < nnodes; i++) {
        delete [] comm_matrix[i];
    }
    delete [] comm_matrix;
}

// write_gp_file:
void write_gpfile(string gpfname, string pngfile, string datfile, int nnodes,
                  double scale_factor)
{
    ofstream gpfile(gpfname.c_str());
    if (gpfile.fail()) {
        throw runtime_error("error at open gpfile");
    }

    gpfile << "set terminal pngcairo enhanced font \"arial," 
           << FONT_SIZE << "\" fontscale 1.0 size " 
           << IMG_WIDTH << ", " << IMG_HEIGHT                   << endl;
    gpfile << "set output \'" << pngfile << "\'"                << endl;
    gpfile << "unset key"                                       << endl;
    gpfile << "set view map"                                    << endl;

    double xtics_end = nnodes - 0.5;

    gpfile << "set xtics -0.5, 1.0, " << xtics_end << " scale " 
           << XTICS_SCALE << ",0"                               << endl;
    gpfile << "set ytics -0.5, 1.0, " << xtics_end << " scale "
           << XTICS_SCALE << ",0"                               << endl;

    gpfile << "set title \"get\""                               << endl;
    gpfile << "set xrange [ -0.5 : " << xtics_end << " ] noreverse nowriteback" 
                                                                << endl;
    gpfile << "set yrange [ -0.5 : " << xtics_end << " ] noreverse nowriteback" 
                                                                << endl;

    gpfile << "set cblabel \"Score\""                           << endl;

    double cbrangemax = scale_factor / (nnodes * nnodes); // get mean

    gpfile << "set cbrange [ 0.00000 : " << cbrangemax
           << " ] noreverse nowriteback"                        << endl;
    gpfile << "set palette rgbformulae 2,-7,-7"                 << endl;

    gpfile << "splot \'" << datfile << "\' matrix with image"   << endl;

    gpfile.close();
}

// compute_scale_factor: 
double compute_scale_factor(int **matrix, int nnodes)
{
    double scale_factor = 0;
    for (int i = 0; i < nnodes; i++) {
        for (int j = 0; j < nnodes; j++) {
            scale_factor += matrix[i][j];
        }
    }
    return scale_factor;
}
