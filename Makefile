CC=g++
CFLAG=-Wall -std=c++11

SRCDIR=src
BINDIR=bin

all: compile

compile: $(SRCDIR)/draw.cpp
	$(CC) $(CFLAGS) $(SRCDIR)/draw.cpp -o $(BINDIR)/draw

clean:
	rm $(BINDIR)/draw
