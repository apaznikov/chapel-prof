# Replace it with your Chapel directory
export CHPL_HOME=$HOME/opt/chapel-1.8.0

export PATH=$CHPL_HOME/bin/linux64:$CHPL_HOME/util:$PATH
export MANPATH=$MANPATH:$CHPL_HOME/man
export CHPL_HOST_PLATFORM=linux64
export CHPL_COMM=gasnet
export CHPL_COMM_SUBSTRATE=ibv
export CHPL_LAUNCHER=gasnetrun_ibv
export GASNET_QUITE=1
